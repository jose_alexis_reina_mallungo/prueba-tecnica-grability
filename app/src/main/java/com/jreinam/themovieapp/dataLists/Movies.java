package com.jreinam.themovieapp.dataLists;

public class Movies {

    private String movieTitle, movieYear, movieType, movieId, movieRate, movieImage, description, movieCategory;

    public void setMovieId(String movieId ) {
        this.movieId = movieId;
    }
    public String getMovieId() {
        return movieId;
    }
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
    public String getMovieTitle() {
        return movieTitle;
    }
    public void setMovieYear(String movieYear) {
        this.movieYear = movieYear;
    }
    public String getMovieYear() {
        return movieYear;
    }
    public void setMovieRate(String movieRate) {
        this.movieRate = movieRate;
    }
    public String getMovieRate() {
        return movieRate;
    }
    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }
    public String getMovieType() {
        return movieType;
    }
    public void setMovieCategory(String movieCategory) {
        this.movieCategory = movieCategory;
    }
    public String getMovieCategory() {
        return movieCategory;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public void setMovieImage(String movieImage) {
        this.movieImage = movieImage;
    }
    public String getMovieImage() {
        return movieImage;
    }
}