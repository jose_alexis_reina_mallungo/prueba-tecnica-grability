package com.jreinam.themovieapp.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.jreinam.themovieapp.data.MoviesDbHelper;
import com.jreinam.themovieapp.interfaces.AddEditMovieListener;

import java.util.ArrayList;

public class AddEditMovieTask extends AsyncTask<ArrayList<ArrayList<String>>, Void, Boolean> {

    public AddEditMovieListener addEditMovieListener;
    private Context context;
    private String tableName;

    public AddEditMovieTask(Context context, String tableName) {
        this.context = context;
        this.tableName = tableName;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        addEditMovieListener.resultAddEditMovie(result);
    }

    @SafeVarargs
    @Override
    protected final Boolean doInBackground(ArrayList<ArrayList<String>>... params) {
        MoviesDbHelper moviesDbHelper = new MoviesDbHelper(context, tableName);
        return moviesDbHelper.createMovie(params[0]);
    }
}
