package com.jreinam.themovieapp.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.jreinam.themovieapp.data.MoviesDbHelper;
import com.jreinam.themovieapp.interfaces.AddEditMovieListener;

import java.util.ArrayList;

public class EditMovieTask extends AsyncTask<ArrayList<String>, Void, Boolean> {

    public AddEditMovieListener addEditMovieListener;
    private Context context;
    private String registerId;

    public EditMovieTask(Context context, String registerId) {
        this.context = context;
        this.registerId = registerId;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        addEditMovieListener.resultAddEditMovie(result);
    }

    @SafeVarargs
    @Override
    protected final Boolean doInBackground(ArrayList<String>... params) {
        MoviesDbHelper moviesDbHelper = new MoviesDbHelper(context, registerId);
        return moviesDbHelper.editMovie(params[0]);
    }
}
