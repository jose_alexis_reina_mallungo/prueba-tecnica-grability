package com.jreinam.themovieapp.tasks;

import android.os.AsyncTask;
import com.jreinam.themovieapp.interfaces.WebServiceListener;
import com.jreinam.themovieapp.requests.WebServiceList;

import java.util.ArrayList;


public class WebServiceReadTask extends AsyncTask<String, Void, ArrayList<ArrayList<String>>> {

    public WebServiceListener webServiceListener;

    public WebServiceReadTask() {
    }

    @Override
    protected void onPostExecute(ArrayList<ArrayList<String>> response) {
        webServiceListener.resultReadWebService(response);
    }

    @Override
    protected ArrayList<ArrayList<String>> doInBackground(String... params) {
        WebServiceList webService = new WebServiceList();
        return webService.read(params[0], params[1], params[2]);
    }
}
