package com.jreinam.themovieapp.tasks;

import android.os.AsyncTask;

import com.jreinam.themovieapp.interfaces.WebServiceDetailListener;
import com.jreinam.themovieapp.interfaces.WebServiceListener;
import com.jreinam.themovieapp.requests.WebServiceDetails;
import com.jreinam.themovieapp.requests.WebServiceList;

import java.util.ArrayList;


public class WebServiceDetailReadTask extends AsyncTask<String, Void, ArrayList<String>> {

    public WebServiceDetailListener webServiceDetailListener;

    public WebServiceDetailReadTask() {
    }

    @Override
    protected void onPostExecute(ArrayList<String> response) {
        webServiceDetailListener.resultReadWebServiceDetail(response);
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        WebServiceDetails webServiceDetails = new WebServiceDetails();
        return webServiceDetails.read(params[0], params[1]);
    }
}
