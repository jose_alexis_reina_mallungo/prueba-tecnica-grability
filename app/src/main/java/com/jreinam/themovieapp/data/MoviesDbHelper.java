package com.jreinam.themovieapp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.jreinam.themovieapp.data.MoviesContract.MoviEntry;

import java.util.ArrayList;

public class MoviesDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Movies.db";
    private String tableName;

    public MoviesDbHelper(Context context, String tableName) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.tableName = tableName;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //create table
        String columns = " ("
                + MoviEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + MoviEntry.ID + " TEXT NOT NULL,"
                + MoviEntry.NAME + " TEXT,"
                + MoviEntry.RATE + " TEXT,"
                + MoviEntry.YEAR + " TEXT,"
                + MoviEntry.IMAGE + " TEXT,"
                + MoviEntry.DESCRIPTION + " TEXT,"
                + MoviEntry.STATE + " TEXT,"
                + MoviEntry.LANGUAGE + " TEXT,"
                + MoviEntry.BUDGET + " TEXT,"
                + MoviEntry.REVENUE + " TEXT,"
                + MoviEntry.GENRE + " TEXT,"
                + "UNIQUE (" + MoviEntry.ID + "))";

        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_MOVIES_POPULAR + columns);
        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_MOVIES_TOP + columns);
        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_MOVIES_UPCOMING + columns);
        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_TV_POPULAR + columns);
        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_TV_TOP + columns);
        sqLiteDatabase.execSQL("CREATE TABLE " + MoviEntry.TABLE_TV_AIR + columns);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public Cursor getAllMovies() {
        return getReadableDatabase()
                .query(
                        tableName,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
    }

    public Cursor getMovieById(String movieId) {
        Cursor c = getReadableDatabase().query(
                tableName,
                null,
                MoviEntry.ID + " LIKE ?",
                new String[]{movieId},
                null,
                null,
                null);
        return c;
    }

    /**
     * update movie when is viewed in list
     */
    private int updateMovies(Movie movie, String movieId) {
        return getWritableDatabase().update(
                tableName,
                movie.toContentValues(),
                MoviEntry.ID + " LIKE ?",
                new String[]{movieId}
        );
    }

    /**
     * update movie when is viewed in details. Add new fields
     */
    private int addFieldsMovies(Movie movie, String movieId) {
        return getWritableDatabase().update(
                tableName,
                movie.toContentValuesToAdd(),
                MoviEntry.ID + " LIKE ?",
                new String[]{movieId}
        );
    }

    /**
     * Save new movies or tv register
     */
    private long saveMovie(Movie movie) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(tableName, null, movie.toContentValues());
    }

    public Boolean createMovie(ArrayList<ArrayList<String>> movies) {

        for (int i = 0; i < movies.size(); i ++){
            String name = movies.get(i).get(0);
            String rate = movies.get(i).get(1);
            String year = movies.get(i).get(2);
            String image = movies.get(i).get(3);
            String description = movies.get(i).get(4);
            String id = movies.get(i).get(5);
            Movie movie = new Movie (name, rate, year, image, description, id);

            if (updateMovies(movie, id) == 0){
                saveMovie(movie);
            }
        }
        return true;
    }

    public Boolean editMovie(ArrayList<String> movie) {

        String id = movie.get(9);
        String state = movie.get(4);
        String language = movie.get(5);
        String budget = movie.get(7);
        String revenue = movie.get(8);
        String genre = movie.get(6);
        String description = movie.get(2);
        Movie mMovie = new Movie (id, state, language, budget, revenue, genre, description);
        addFieldsMovies(mMovie, id);

        return true;
    }
}
