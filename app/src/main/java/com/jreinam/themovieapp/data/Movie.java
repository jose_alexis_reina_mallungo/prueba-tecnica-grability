package com.jreinam.themovieapp.data;

import android.content.ContentValues;
import com.jreinam.themovieapp.data.MoviesContract.MoviEntry;

/**
 * Movie entity
 */
public class Movie {
    private String id;
    private String name;
    private String rate;
    private String year;
    private String image;
    private String description;
    private String state;
    private String language;
    private String budget;
    private String revenue;
    private String genre;

    public Movie(String name, String rate, String year, String image, String description, String id) {

        this.id = id;
        this.name = name;
        this.rate = rate;
        this.year = year;
        this.image = image;
        this.description = description;
    }

    public Movie(String movieId, String state, String lanaguage, String budget, String revenue, String genre, String decription) {

        this.state = state;
        this.language = lanaguage;
        this.budget = budget;
        this.revenue = revenue;
        this.genre = genre;
        this.description = decription;
        this.id = movieId;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(MoviEntry.ID, id);
        values.put(MoviEntry.NAME, name);
        values.put(MoviEntry.RATE, rate);
        values.put(MoviEntry.YEAR, year);
        values.put(MoviEntry.IMAGE, image);
        values.put(MoviEntry.DESCRIPTION, description);
        return values;
    }

    public ContentValues toContentValuesToAdd() {
        ContentValues values = new ContentValues();
        values.put(MoviesContract.MoviEntry.ID, id);
        values.put(MoviEntry.STATE, state);
        values.put(MoviEntry.LANGUAGE, language);
        values.put(MoviEntry.BUDGET, budget);
        values.put(MoviEntry.REVENUE, revenue);
        values.put(MoviEntry.GENRE, genre);
        return values;
    }
}