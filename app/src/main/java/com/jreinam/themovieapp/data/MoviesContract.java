package com.jreinam.themovieapp.data;

import android.provider.BaseColumns;

/**
 * Schema of data base to movies
 */
public class MoviesContract {
    public static abstract class MoviEntry implements BaseColumns {
        public static final String TABLE_MOVIES_POPULAR ="moviepopular";
        public static final String TABLE_MOVIES_TOP ="movietop_rated";
        public static final String TABLE_MOVIES_UPCOMING ="movieupcoming";
        public static final String TABLE_TV_POPULAR ="tvpopular";
        public static final String TABLE_TV_TOP ="tvtop_rated";
        public static final String TABLE_TV_AIR ="tvon_the_air";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String RATE = "rate";
        public static final String YEAR = "year";
        public static final String IMAGE = "image";
        public static final String DESCRIPTION = "description";
        public static final String STATE = "state";
        public static final String LANGUAGE = "language";
        public static final String BUDGET = "budget";
        public static final String REVENUE = "revenue";
        public static final String GENRE = "genre";
    }
}
