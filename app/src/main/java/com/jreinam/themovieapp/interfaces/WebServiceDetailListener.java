package com.jreinam.themovieapp.interfaces;

import java.util.ArrayList;

public interface WebServiceDetailListener {
    void resultReadWebServiceDetail(ArrayList<String> response);
}
