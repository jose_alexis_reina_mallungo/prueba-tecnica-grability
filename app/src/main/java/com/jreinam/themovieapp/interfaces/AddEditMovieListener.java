package com.jreinam.themovieapp.interfaces;

public interface AddEditMovieListener {
    void resultAddEditMovie(Boolean response);
}
