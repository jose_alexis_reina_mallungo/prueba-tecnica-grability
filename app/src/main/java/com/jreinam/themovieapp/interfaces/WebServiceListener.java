package com.jreinam.themovieapp.interfaces;

import java.util.ArrayList;

public interface WebServiceListener {
    void resultReadWebService(ArrayList<ArrayList<String>> response);
}
