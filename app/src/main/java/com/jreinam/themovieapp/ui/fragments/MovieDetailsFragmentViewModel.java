package com.jreinam.themovieapp.ui.fragments;

import android.content.Context;
import android.databinding.ObservableField;
import com.jreinam.themovieapp.interfaces.AddEditMovieListener;
import com.jreinam.themovieapp.tasks.EditMovieTask;

import java.util.ArrayList;

public class MovieDetailsFragmentViewModel implements AddEditMovieListener {
    private String programType;
    private String category;
    private Context context;

    public final ObservableField<String> episodes = new ObservableField<>("");
    public final ObservableField<String> season = new ObservableField<>("");
    public final ObservableField<String> rate = new ObservableField<>("");
    public final ObservableField<String> description = new ObservableField<>("");
    public final ObservableField<String> status = new ObservableField<>("");
    public final ObservableField<String> language = new ObservableField<>("");
    public final ObservableField<String> genre = new ObservableField<>("");
    public final ObservableField<String> budget = new ObservableField<>("");
    public final ObservableField<String> revenue = new ObservableField<>("");

    public MovieDetailsFragmentViewModel(String programType, String category, Context context) {
        this.programType = programType;
        this.category = category;
        this.context = context;
        load();
    }

    private void load() {
        if (programType.equals("tv")) {
            episodes.set("Número de episodios");
            season.set("Número de temporadas");
        } else {
            episodes.set("Presuspuesto");
            season.set("Ingresos");
        }
    }

    public void updateMovie(ArrayList<String> response) {
        EditMovieTask editMovieTask = new EditMovieTask(context, programType + category);
        editMovieTask.addEditMovieListener = MovieDetailsFragmentViewModel.this;
        editMovieTask.execute(response);
    }

    public void showResult(String mrate, String desc, String stat, String lan, String gen, String bud, String rev) {
        rate.set(mrate);
        description.set(desc);
        status.set(stat);
        language.set(lan);
        genre.set(gen);
        budget.set(bud);
        revenue.set(rev);
    }

    @Override
    public void resultAddEditMovie(Boolean response) {

    }
}
