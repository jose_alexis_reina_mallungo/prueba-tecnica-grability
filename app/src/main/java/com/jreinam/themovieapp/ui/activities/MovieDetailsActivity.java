package com.jreinam.themovieapp.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.jreinam.themovieapp.R;
import com.jreinam.themovieapp.databinding.ActivityMovieDetailsBinding;
import com.jreinam.themovieapp.ui.fragments.MovieDetailsFragment;
import com.squareup.picasso.Picasso;

public class MovieDetailsActivity extends AppCompatActivity implements MovieDetailsFragment.MovieDetailsFragmentListener {
    private ActivityMovieDetailsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_movie_details);

        setSupportActionBar(binding.toolbar);

        Intent intent = getIntent();
        String id = intent.getStringExtra("extra_movie_id");
        String programType = intent.getStringExtra("extra_movie_type");
        String category = intent.getStringExtra("extra_movie_category");

        MovieDetailsFragment fragment = MovieDetailsFragment.newInstance(id, programType, category);
        getSupportFragmentManager().beginTransaction().add(R.id.movie_detail_container, fragment).commit();
    }

    @Override
    public void setMovie(String name, String image) {
        Picasso.with(this)
                .load("https://image.tmdb.org/t/p/w500" + image)
                .resize(500, 500)
                .centerCrop()
                .into(binding.movieImage);
        binding.toolbar.setTitle(name);
    }
}
