package com.jreinam.themovieapp.ui.fragments;

import android.content.Context;

import com.jreinam.themovieapp.adapters.MoviesAdapter;
import com.jreinam.themovieapp.interfaces.AddEditMovieListener;
import com.jreinam.themovieapp.dataLists.Movies;
import com.jreinam.themovieapp.tasks.AddEditMovieTask;

import java.util.ArrayList;
import java.util.List;

public class MoviesFragmentViewModel implements  AddEditMovieListener {
    private String programType;
    private String category;
    private Context context;

    public MoviesFragmentViewModel(String programType, String category, Context context) {
        this.programType = programType;
        this.category = category;
        this.context = context;
    }

    //save da in database when webservice is avaailable
    public void saveDb (ArrayList<ArrayList<String>> response){
        AddEditMovieTask addEditMovieTask = new AddEditMovieTask(context, programType + category);
        addEditMovieTask.addEditMovieListener = MoviesFragmentViewModel.this;
        addEditMovieTask.execute(response);
    }


    public void fillList(ArrayList<ArrayList<String>> response, MoviesAdapter adapter,  List<Movies> listMovies){
        int movieQty = response.size();
        for (int i = 0; i < movieQty; i++){
            showResults (response.get(i).get(0), response.get(i).get(1), response.get(i).get(2),
                    response.get(i).get(3), response.get(i).get(4), response.get(i).get(5), listMovies);
        }
        adapter.notifyDataSetChanged();
    }

    //set the list adapter
    public void showResults (String name, String rate, String year, String image, String desc, String movieId, List<Movies> listMovies){
        Movies movies = new Movies();
        movies.setMovieType(programType);
        movies.setMovieCategory(category);
        movies.setMovieTitle(name);
        movies.setMovieRate(rate);
        movies.setMovieYear(year);
        movies.setMovieImage("https://image.tmdb.org/t/p/w500" + image);
        movies.setDescription(desc);
        movies.setMovieId(movieId);
        listMovies.add(movies);
    }


    @Override
    public void resultAddEditMovie(Boolean response) {

    }
}


