package com.jreinam.themovieapp.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.jreinam.themovieapp.R;
import com.jreinam.themovieapp.adapters.MenuExpandableListAdapter;
import com.jreinam.themovieapp.ui.fragments.MoviesFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView mTitle;
    private ExpandableListView expListView;
    private DrawerLayout mDrawerLayout;
    private List<String> listDataHeader;
    private int [] listImageHeader = new int[5];
    private HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("");

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        String title = "Inicio";
        mTitle.setText(title);

        drawerLatout(toolbar);
        prepareListData();
        expandableListMenu();
    }

    private void expandableListMenu(){
        expListView = (ExpandableListView) findViewById(R.id.left_drawer);
        ExpandableListAdapter listAdapter = new MenuExpandableListAdapter(this, listDataHeader, listDataChild, listImageHeader);
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                selectItem(childPosition, groupPosition);
                mDrawerLayout.closeDrawer(expListView);
                return false;
            }
        });
    }
    private void drawerLatout(Toolbar toolbar){
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close)  /* "close drawer" description for accessibility */ {
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    /** Swaps fragments in the main content view */
    private void selectItem(int childPosition, int groupPosition) {
        //update the title, and close the drawer, if the group not has children, take the group label as title
        setTitle(listDataHeader.get(groupPosition) + (" - ") + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
        mDrawerLayout.closeDrawer(expListView);
        expListView.collapseGroup(groupPosition);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new MoviesFragment();
        Bundle  bundle = new Bundle();
        switch (groupPosition){
            case 0:
                switch (childPosition){
                    case 0:
                        bundle.putString("program", "movie");
                        bundle.putString("category", "popular");
                        break;
                    case 1:
                        bundle.putString("program", "movie");
                        bundle.putString("category", "top_rated");
                        break;
                    case 2:
                        bundle.putString("program", "movie");
                        bundle.putString("category", "upcoming");
                        break;
                }
                break;
            case 1:
                switch (childPosition){
                    case 0:
                        bundle.putString("program", "tv");
                        bundle.putString("category", "popular");
                        break;
                    case 1:
                        bundle.putString("program", "tv");
                        bundle.putString("category", "top_rated");
                        break;
                    case 2:
                        bundle.putString("program", "tv");
                        bundle.putString("category", "on_the_air");
                        break;
                }
                break;
            default:
                mDrawerLayout.closeDrawer(expListView);
                break;
        }
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle.setText(title.toString());
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding group data
        listDataHeader.add("Peliculas");
        listDataHeader.add("Series");

        listImageHeader[0] = R.drawable.movie;
        listImageHeader[1] = R.drawable.tv;


        // Adding child data
        List<String> movie = new ArrayList<>();
        movie.add("Popular");
        movie.add("Mejor valoradas");
        movie.add("Próximamente");

        List<String> tv = new ArrayList<>();
        tv.add("Popular");
        tv.add("Mejor valoradas");
        tv.add("En televisión");

        listDataChild.put(listDataHeader.get(0), movie); // Header, Child data
        listDataChild.put(listDataHeader.get(1), tv);
    }
}
