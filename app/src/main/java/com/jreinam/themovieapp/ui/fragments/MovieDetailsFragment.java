package com.jreinam.themovieapp.ui.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jreinam.themovieapp.data.MoviesContract;
import com.jreinam.themovieapp.data.MoviesDbHelper;
import com.jreinam.themovieapp.databinding.FragmentMovieDetailsBinding;
import com.jreinam.themovieapp.interfaces.WebServiceDetailListener;
import com.jreinam.themovieapp.tasks.WebServiceDetailReadTask;

import java.util.ArrayList;

public class MovieDetailsFragment extends Fragment implements WebServiceDetailListener {
    private String movieId, programType, category;
    private FragmentMovieDetailsBinding binding;
    private MovieDetailsFragmentListener listener;
    private MovieDetailsFragmentViewModel movieDetailsFragmentViewModel;

    public MovieDetailsFragment() {
        // Required empty public constructor
    }

    public static MovieDetailsFragment newInstance(String movieId, String programType, String category) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        Bundle args = new Bundle();
        args.putString("movieId", movieId);
        args.putString("programType", programType);
        args.putString("category", category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            movieId = bundle.getString("movieId");
            programType = bundle.getString("programType");
            category = bundle.getString("category");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        movieDetailsFragmentViewModel = new MovieDetailsFragmentViewModel(programType, category, getActivity());
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false);
        binding.setViewModel(movieDetailsFragmentViewModel);
        binding.card.setVisibility(View.GONE);
        requestMovies();
        return binding.getRoot();
    }

    public interface MovieDetailsFragmentListener {
        void setMovie(String name, String image);
    }

    private void requestMovies() {
        WebServiceDetailReadTask webServiceDetailReadTask = new WebServiceDetailReadTask();
        webServiceDetailReadTask.webServiceDetailListener = MovieDetailsFragment.this;
        webServiceDetailReadTask.execute(programType, movieId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MovieDetailsFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(" must implement r");
        }
    }

    @Override
    public void resultReadWebServiceDetail(ArrayList<String> response) {
        String name ="";
        String image ="";
        binding.progressBar.setVisibility(View.GONE);
        binding.card.setVisibility(View.VISIBLE);
        if (response.size() > 0) {

            name = response.get(0);
            image = response.get(3);

            movieDetailsFragmentViewModel.showResult(response.get(1), response.get(2), response.get(4), response.get(5),
                        response.get(6), response.get(7), response.get(8));

            response.add(movieId);
            movieDetailsFragmentViewModel.updateMovie(response);
        } else {
            MoviesDbHelper moviesDbHelper = new MoviesDbHelper(getActivity(), programType + category);
            Cursor cursor = moviesDbHelper.getMovieById(movieId);
            if (cursor != null && cursor.moveToFirst()) {

                name = cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.NAME));
                image = cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.IMAGE));

                movieDetailsFragmentViewModel.showResult(
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.RATE)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.STATE)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.LANGUAGE)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.GENRE)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.BUDGET)),
                        cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.REVENUE))
                );
            }
        }

        if (listener != null) {
            listener.setMovie(name, image);
        }
    }
}
