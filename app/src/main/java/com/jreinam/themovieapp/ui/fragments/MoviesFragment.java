package com.jreinam.themovieapp.ui.fragments;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.jreinam.themovieapp.R;
import com.jreinam.themovieapp.adapters.MoviesAdapter;
import com.jreinam.themovieapp.data.MoviesContract;
import com.jreinam.themovieapp.data.MoviesDbHelper;
import com.jreinam.themovieapp.databinding.FragmentMoviesBinding;
import com.jreinam.themovieapp.interfaces.WebServiceListener;
import com.jreinam.themovieapp.dataLists.Movies;
import com.jreinam.themovieapp.tasks.WebServiceReadTask;

import java.util.ArrayList;
import java.util.List;

public class MoviesFragment extends Fragment implements WebServiceListener {
    private MoviesAdapter adapter;
    private List<Movies> listMovies;
    private String programType, category;
    private int page;
    private Boolean flag_loading;
    private ProgressDialog dialog;
    private FragmentMoviesBinding binding;
    private MoviesFragmentViewModel moviesFragmentViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null){
            programType = bundle.getString("program");
            category = bundle.getString("category");
            page = 1;
        }
        flag_loading = false;

        moviesFragmentViewModel = new MoviesFragmentViewModel(programType, category, getActivity());
        binding = FragmentMoviesBinding.inflate(inflater, container, false);
        binding.setViewModel(moviesFragmentViewModel);

        initNoList();
        initProgressDialog();
        initList();
        requestMovies();

        return binding.getRoot();
    }

    private void initNoList(){
        String noReports = "No hay listado disponible";
        binding.noListTv.setText(noReports);
        binding.noListTv.setVisibility(View.INVISIBLE);
    }
    private void initProgressDialog(){
        dialog = new ProgressDialog(getActivity(), R.style.StyledDialog);
        dialog.setMessage("");
        dialog.setCancelable(false);
    }

    private void initList(){
        listMovies = new ArrayList<>();
        adapter = new MoviesAdapter(getActivity(), listMovies);
        ListView list =  binding.list;
        list.setAdapter(adapter);
        //get movies or tv by page, controlled by scroll of list view
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if(!flag_loading) {
                        flag_loading = true;
                        dialog.show();
                        page = page + 1;
                        requestMovies();
                    }
                }
            }
        });
    }

    // make a ws request in an async task
    private void requestMovies(){
        WebServiceReadTask webServiceReadTask = new WebServiceReadTask();
        webServiceReadTask.webServiceListener = MoviesFragment.this;
        webServiceReadTask.execute(programType, category, String.valueOf(page));
    }

    //listener of ws response
    @Override
    public void resultReadWebService(ArrayList<ArrayList<String>> response) {
        binding.progressBar.setVisibility(View.GONE);
        dialog.dismiss();
        if (response.size() > 0){
            moviesFragmentViewModel.saveDb(response);
            moviesFragmentViewModel.fillList(response, adapter, listMovies);
            flag_loading = false;
        }
        else {
            flag_loading = true;
            MoviesDbHelper moviesDbHelper = new MoviesDbHelper(getActivity(),  programType + category);
            Cursor cursor = moviesDbHelper.getAllMovies();
            if (cursor != null && cursor.moveToFirst()){
                do {
                    moviesFragmentViewModel.showResults(cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.NAME)),
                            cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.RATE)),
                            cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.YEAR)),
                            cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.IMAGE)),
                            cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.DESCRIPTION)),
                            cursor.getString(cursor.getColumnIndex(MoviesContract.MoviEntry.ID)),
                            listMovies
                    );
                } while (cursor.moveToNext());
                adapter.notifyDataSetChanged();
            }
            // noListTv.setVisibility(View.VISIBLE);
        }
    }
}
