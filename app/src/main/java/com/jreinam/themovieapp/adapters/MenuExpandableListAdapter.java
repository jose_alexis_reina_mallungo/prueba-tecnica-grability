package com.jreinam.themovieapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jreinam.themovieapp.R;

import java.util.HashMap;
import java.util.List;

public class MenuExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader;
    private HashMap<String, List<String>> _listDataChild;
    private int[] listImageHeader;


    public MenuExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData, int[] listImageHeader) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.listImageHeader = listImageHeader;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.menu_list_item_child, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        String tag = String.valueOf(groupPosition) + String.valueOf(childPosition);
        txtListChild.setTag(tag);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.menu_list_item_group, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);
        RelativeLayout groupCont = (RelativeLayout)convertView.findViewById(R.id.groupCont);
        String tag = String.valueOf(groupPosition);
        groupCont.setTag("rl" + tag);
        lblListHeader.setTag("tv" + tag);
        ImageView imageViewIndicator = (ImageView) convertView.findViewById(R.id.indicator);
        ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.menu_icon);
        imageViewIcon.setImageResource(this.listImageHeader[groupPosition]);

        //change the icon when is expanded or collapsed
        if (getChildrenCount(groupPosition) > 0){
            if (isExpanded){
                imageViewIndicator.setImageResource(R.drawable.menuexpanded);
            }
            else{
                imageViewIndicator.setImageResource(R.drawable.menucollapsed);
            }
        }

        if (groupPosition == 4){
            imageViewIndicator.setImageResource(0);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}