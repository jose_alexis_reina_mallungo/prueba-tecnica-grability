package com.jreinam.themovieapp.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jreinam.themovieapp.lists.ListItemMovies;
import com.jreinam.themovieapp.dataLists.Movies;

import java.util.List;


public class MoviesAdapter extends BaseAdapter {

    private Activity activity;
    private List<Movies> movies;


    public MoviesAdapter(Activity activity, List<Movies> movies){
        this.activity = activity;
        this.movies = movies;
    }

    public int getCount() {
        return movies.size();
    }

    public Object getItem(int position) {
        return movies.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return new ListItemMovies(activity, movies.get(position));
    }
}