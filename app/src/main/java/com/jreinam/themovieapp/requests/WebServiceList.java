package com.jreinam.themovieapp.requests;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WebServiceList {
    private static final String BASEURL = "https://api.themoviedb.org/3/";
    private OkHttpClient client;

    public WebServiceList(){
        client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    public ArrayList<ArrayList<String>> read (String programType, String category, String page){
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        Request request = new Request.Builder()
                .url(getUrl(programType, category, page))
                .addHeader("content-type", "application/json")
                .addHeader("accept", "application/json")
                .get()
                .build();
        try {
            Response response = client.newCall(request).execute();
            String jsonData;
            JSONObject jsonObject;
            try {
                jsonData = response.body().string();
                try {
                    jsonObject = new JSONObject(jsonData);
                    String movieTitle, movieYear, movieRate = "", movieImage = "", movieDescription= "", movieId = "";
                    int moviesQty = jsonObject.getJSONArray("results").length();
                    for (int i = 0; i < moviesQty; i++){
                        ArrayList<String> unitResult = new ArrayList<>();
                        try{
                            movieTitle = jsonObject.getJSONArray("results").getJSONObject(i).getString("title");
                        }catch (JSONException e){
                            movieTitle = jsonObject.getJSONArray("results").getJSONObject(i).getString("name");
                        }

                        try{
                            movieYear = jsonObject.getJSONArray("results").getJSONObject(i).getString("release_date");
                        }catch (JSONException e) {
                            movieYear = jsonObject.getJSONArray("results").getJSONObject(i).getString("first_air_date");
                        }

                        try{
                            movieRate = jsonObject.getJSONArray("results").getJSONObject(i).getString("vote_average");
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try{
                            movieImage = jsonObject.getJSONArray("results").getJSONObject(i).getString("poster_path");
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try{
                            movieDescription = jsonObject.getJSONArray("results").getJSONObject(i).getString("overview");
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try{
                            movieId = jsonObject.getJSONArray("results").getJSONObject(i).getString("id");
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        unitResult.add(movieTitle);
                        unitResult.add(movieRate);
                        unitResult.add(movieYear);
                        unitResult.add(movieImage);
                        unitResult.add(movieDescription);
                        unitResult.add(movieId);
                        result.add(unitResult);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private String getUrl(String programType, String category, String page) {

        String api_key = "0fa47497cd42c2aa0818a0e1cd3d8967";
        String language = "es-ES";

        return BASEURL + programType + "/" + category + "?api_key=" + api_key + "&language=" + language + "&page=" + page;
    }
}
