package com.jreinam.themovieapp.requests;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WebServiceDetails {
    private static final String BASEURL = "https://api.themoviedb.org/3/";
    private OkHttpClient client;

    public WebServiceDetails(){
        client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    public ArrayList<String> read (String programType, String id){
        ArrayList<String> result = new ArrayList<>();
        System.out.println("geturl: " + getUrl(programType, id));
        Request request = new Request.Builder()
                .url(getUrl(programType, id))
                .addHeader("content-type", "application/json")
                .addHeader("accept", "application/json")
                .get()
                .build();
        try {
            Response response = client.newCall(request).execute();
            String jsonData;
            JSONObject jsonObject;
            try {
                jsonData = response.body().string();
                try {
                    jsonObject = new JSONObject(jsonData);

                    try{
                        result.add(jsonObject.getString("title"));
                    }catch (JSONException e){
                        result.add(jsonObject.getString("name"));
                    }

                    try{
                        result.add(jsonObject.getString("vote_average"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        result.add(jsonObject.getString("overview"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        result.add(jsonObject.getString("poster_path"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        result.add(jsonObject.getString("status"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        result.add(jsonObject.getString("original_language"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        int genreQty = jsonObject.getJSONArray("genres").length();
                        String genre = "";
                        for (int i = 0; i < genreQty; i++){
                            genre += jsonObject.getJSONArray("genres").getJSONObject(i).getString("name") + " ";
                        }
                        result.add(genre);
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add("");
                    }

                    try{
                        result.add(jsonObject.getString("budget"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add(jsonObject.getString("number_of_episodes"));
                    }

                    try{
                        result.add(jsonObject.getString("revenue"));
                    }catch (JSONException e) {
                        e.printStackTrace();
                        result.add(jsonObject.getString("number_of_seasons"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private String getUrl(String programType, String id) {

        String api_key = "0fa47497cd42c2aa0818a0e1cd3d8967";
        String language = "es-ES";

        return BASEURL + programType + "/" + id + "?api_key=" + api_key + "&language=" + language;
    }
}
