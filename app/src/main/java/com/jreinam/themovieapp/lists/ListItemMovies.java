package com.jreinam.themovieapp.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jreinam.themovieapp.dataLists.Movies;
import com.jreinam.themovieapp.R;
import com.jreinam.themovieapp.ui.activities.MovieDetailsActivity;
import com.squareup.picasso.Picasso;

public class ListItemMovies extends LinearLayout {

    private Movies movies;
    private Activity activity;

    public ListItemMovies(Activity activity, Movies movies) {
        super(activity);
        this.movies = movies;
        this.activity = activity;
        init();
    }

    private void init(){

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater)getContext().getSystemService(infService);
        li.inflate(R.layout.item_list_movies, this, true);

        CardView card = (CardView) findViewById(R.id.card);
        ImageView movieImage = (ImageView) findViewById(R.id.movieImage);
        TextView title = (TextView) findViewById(R.id.title);
        TextView year = (TextView) findViewById(R.id.year);
        TextView rate = (TextView) findViewById(R.id.rate);
        TextView description = (TextView) findViewById(R.id.description);

        title.setText(movies.getMovieTitle());
        year.setText(movies.getMovieYear());
        rate.setText(movies.getMovieRate());
        String text;
        if (movies.getDescription().length() > 160){
            text = movies.getDescription().substring(0, 160) + ("...");
        }
        else{
            text = movies.getDescription();
        }

        description.setText(text);

        Picasso.with(activity)
                .load(movies.getMovieImage())
                .resize(500, 500)
                .centerCrop()
                .into(movieImage);

        card.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailScreen();
            }
        });
    }
    private void showDetailScreen() {
        Intent intent = new Intent(activity, MovieDetailsActivity.class);
        intent.putExtra("extra_movie_id", movies.getMovieId());
        intent.putExtra("extra_movie_type", movies.getMovieType());
        intent.putExtra("extra_movie_category", movies.getMovieCategory());
        activity.startActivity(intent);
    }
}
